-- 1.1 Create a class_db database

CREATE DATABASE class_db;


-- 1.2 Inside class_db, create students table
-- 1.3 In the "students" table, create the following columns
	-- id INT(15) NOT NULL
	-- last_name VARCHAR(50) NOT NULL
	-- first_name VARCHAR(50) NOT NULL
	-- contact INT NOT NULL
	-- email VARCHAR(50)
	-- address VARCHAR(50)
	-- course_id INT NOT NULL

CREATE TABLE students (
	id INT(15) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	first_name VARCHAR(50) NOT NULL,
	contact INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	course_id INT NOT NULL
);



-- 1.4 In the same class_db, create courses table
-- 1.5 In the "courses" table, create the following columns
	-- id INT(15) NOT NULL
	-- course_name VARCHAR(50) NOT NULL
	-- student_id INT NOT NULL

CREATE TABLE courses (
	id INT(15) NOT NULL,
	course_name VARCHAR(50) NOT NULL,
	student_id INT NOT NULL
);


-- 2.1 In the "students" and "courses" table add the "id" field as the primary key.

ALTER TABLE students
	ADD PRIMARY KEY (id);

ALTER TABLE courses
	ADD PRIMARY KEY (id);


-- 2.2 In both tables, modify the "id" field from INT(15) to INT (10) and auto increment both the "id" fields.

ALTER TABLE students
	MODIFY id INT(10) NOT NULL AUTO_INCREMENT;

ALTER TABLE courses
	MODIFY id INT(10) NOT NULL AUTO_INCREMENT;

-- 2.3 Set the "courses_id" and "student_id" as foreign keys.

ALTER TABLE students
	ADD FOREIGN KEY (course_id) REFERENCES courses(id);

ALTER TABLE courses
	ADD FOREIGN KEY (student_id) REFERENCES students(id);

-- 3.1 Add constraints for the "courses" table.
-- 3.2 Use UPDATE CASCASE referencing the "id" in the "students" table

ALTER TABLE courses
	ADD CONSTRAINT fk_student_id
		FOREIGN KEY (student_id) REFERENCES students(id)
		ON UPDATE CASCADE;